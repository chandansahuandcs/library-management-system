const express = require("express");
const router = express.Router();
const controller=require('../controller/user.controller')
router.post('/RegisterUser',controller.Register);
router.get('/VerifyUser',controller.login);
module.exports=router;