const express=require('express')
const {protect}=require('../authentication/auth')
const route=express.Router()
const bookController=require('../controller/book.controller')
route.post('/CreateBook',protect,bookController.createBook)
route.put('/UpdateBook',protect,bookController.updateBook);
route.get('/GetAllBookDetails',protect,bookController.getAllBookDetails)
module.exports=route