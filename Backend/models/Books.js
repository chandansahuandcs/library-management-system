const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bookSchema = new Schema(
    {
        Name: String,
        Author:String,
        CurrentAvailable:Number,
        IsActive:Boolean,
        IsDelete:Boolean,
        ImgUrl:String,

    }, {
    timestamps: true,
}
)
const Books=mongoose.model("Books",bookSchema)
module.exports=Books