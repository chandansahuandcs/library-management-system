const mongoose=require('mongoose')
const Schema=mongoose.Schema;
const BookTransactionsSchema=new Schema(
    {
        UserId:{
            type:mongoose.Types.ObjectId,
            require:true,
            ref:'Users',
        },
        BookId:{
            type:mongoose.Types.ObjectId,
            require:true,
            ref:'Books',
        },
        date:{
            type: Date,
            required: true
        },
        transactionType: {
            type: String,
            required: true,
            enum: ['borrowed', 'returned'],
        }
    },{
        timestamps:true
    }
)
const BookTransaction = mongoose.model('BookTransaction', BookTransactionsSchema);
module.exports = BookTransaction;