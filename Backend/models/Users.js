const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const userSchema = new Schema(
    {
    UserName: String,
    Name: String,
    Email: String,
    ContactNumber: Number,
    ImgUrl:String,
    Role: String,
    Password: String,
    IsActive: Boolean,
    IsDelete: Boolean,
    },
    {
        timestamps: true,
    })
const User = mongoose.model("User", userSchema);
module.exports=User