const jwt = require("jsonwebtoken");
const User = require('../models/Users');
const JWT_SECRET = require('../config/config');
const { use } = require("../routes/user.route");
const protect = async (req, res, next) => {
    let token;
    if (
        req.headers.authorization &&
        req.headers.authorization.startsWith("Bearer")
    ) {
        try {
            token = req.headers.authorization.split(" ")[1];
            const decoded = jwt.verify(token, JWT_SECRET);
            console.log("decoded", decoded);
            const user = await User.findById(decoded.userId).select("-Password");
            console.log("prinintg here user data is",user)
            if (user && user.IsActive) {
                req.user=user
                next();
            }else{
               return res.status(401).json({ error: "User not found or not active." });
            }
 
           
          } catch (error) {
            console.error(error);
      
            return res.status(401).json({
              error: "Not authorized, token failed",
            });
          }
    }
    else{
        return res.status(401).json({
            error: "Not authorized, token Not Found",
          });
    }
}
module.exports = { protect }