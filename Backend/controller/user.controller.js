const User = require('../models/Users')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_SECRET = require('../config/config');
const Register = async (req, res) => {
    try {
        const { UserName, Name, Email, ContactNumber, Role, Password } = req.body;
        const existingUser = await User.findOne({ Email });
        if (existingUser) {
            return res.status(400).json({ error: 'User with this email already exists' });
        }
        const saltRounds = 10;
        const hashedPassword = await bcrypt.hash(Password, saltRounds);
        const newUser = new User({
            UserName,
            Name,
            Email,
            ContactNumber,
            Role,
            Password: hashedPassword,
            IsActive: true,
            IsDelete: false,
        });
        await newUser.save();
        res.status(201).json({ message: 'User registered successfully', user: newUser });

    } catch (error) {
        return res.status(500).json({ error: error })
    }
}
const login = async (req, res) => {
    try {
        const { Email, Password } = req.body;
        const user = await User.findOne({ Email });
        if (!user) {
            return res.status(401).json({ error: 'Invalid Email' });
        }
        const passwordMatch = await bcrypt.compare(Password, user.Password);
        if (!passwordMatch) {
            return res.status(401).json({ error: 'Invalid Password' });
        }
        const token = jwt.sign({ userId: user._id, email: user.Email }, JWT_SECRET, { expiresIn: '30d' });
        return res.status(200).json({ token, userId: user._id, message: 'Login successful' });
    } catch(error) {
        console.log(error)
        return res.status(500).json({ error: error });
    }
}
const updateDetails = async (req, res) => {
    try {

    } catch {

    }
}
const validateUsername = async (req, res) => {
    try {

    } catch {

    }
}
const deleteUser = async (req, res) => {
    try {

    } catch {

    }
}
const getAllUserDetails = async (req, res) => {

}
module.exports = { Register, login, updateDetails, validateUsername, deleteUser, getAllUserDetails }