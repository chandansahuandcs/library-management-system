const Books=require('../models/Books');
const createBook=async(req,res)=>{
    try{
        const {Author,CurrentAvailable,ImgUrl}=req.body
        const newBooks=new Books({
            Author,
            CurrentAvailable,
            ImgUrl,
            IsActive:true,
            IsDelete:false,   
        })
        await newBooks.save()
        return res.status(201).json({message:'Book Created Sucessfully',Book:Books})

    }catch(error){
        console.log(error)
        return res.status(500).json({ error: "Try again Later" })
    }
}
const updateBook=async(req,res)=>{

}
const deletebook=async(req,res)=>{

}
const getAllBookDetails=async(req,res)=>{

}
const updateBookQuantity=async(req,res)=>{

}
module.exports={createBook,updateBook,deletebook,getAllBookDetails,updateBookQuantity}
