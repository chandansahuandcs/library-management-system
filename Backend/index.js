const express=require('express');
const cors=require('cors');
const app=express();
const connectDb=require('./config/connection');
const userRoute=require('./routes/user.route');
const BookRoute=require('./routes/book.route');
const BookTransactionRoute=require('./routes/transaction.route');
// Connect to MongoDB
connectDb();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//declaration of Port
const port=4000
app.listen(port,()=>{
    console.log("Port running in 4000")
})
app.use("/api/user",userRoute);
app.use("/api/Book",BookRoute);
// app.use("/api/bookTransaction",BookTransactionRoute)