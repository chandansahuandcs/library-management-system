import React,{ createContext, useContext, useState,useCallback } from 'react'
const LoadingContext = createContext();


const LoadingProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const updateLoading = useCallback((value) => {
    setLoading(value);
  }, []);
  return (
    <LoadingContext.Provider value={{ loading, updateLoading }}>
      {children}
    </LoadingContext.Provider>
  );
}

const useLoading = () => {
  const context = useContext(LoadingContext);


  if (!context) {
    throw new Error('useLoading must be used within a LoadingProvider');
  }


  return context;
};
export { LoadingProvider, useLoading };