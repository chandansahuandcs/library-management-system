import React from 'react'
import loader from './Loader';
import { useLoading } from '../Context/LoaderContext';
import { useLocation } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Sidebar from './Sidebar';

const layout = ({ children }) => {
    const location = useLocation();
    const showHeaderAndSidebar = !['/login', '/error', '/'].includes(location.pathname);
    const { loading } = useLoading();
    return (
        <>
            {loading && <LoaderComponent />}
            <div className="page-wrapper">
                {showHeaderAndSidebar && (
                    <div>
                        <Header />
                        <div className="flex">
                            <Sidebar />
                            <div className="flex-1">
                                {children}
                            </div>
                        </div>
                    </div>
                )}
                <Footer />
            </div>
        </>
    )
}
export default layout
