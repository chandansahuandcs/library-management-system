import React from 'react'

const Loader = () => {
  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-gray-300 bg-opacity-40">
    <div className="animate-spin rounded-full border-t-4 border-blue-500 border-solid h-16 w-16">
     
    </div>
  </div>

  )
}

export default Loader
