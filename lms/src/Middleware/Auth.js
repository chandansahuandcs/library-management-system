import axios from 'axios';

const api = axios.create({
    baseURL:'http://localhost:4000/' // Set your API base URL here
});
const setAuthToken = (token) => {
    if (token) {
        api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    } else {
        delete api.defaults.headers.common['Authorization'];
    }
};
api.interceptors.response.use(
    (response) => {
      // Handle success response
      return response;
    },
    (error) => {
   
      if (error.response && error.response.status === 401) {
        localStorage.removeItem('token');
        window.location.href = '/';
      }
      return Promise.reject(error);
    }
  );
module.exports={api,setAuthToken}